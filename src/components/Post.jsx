import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import Card from "./Card";
import PostImg from "../images/Rectangle5.png";
import ProfileImg from "../images/Rectangle3.1.png";
import Secondpost from "../images/2ndpost.png";
import ThirdPost from "../images/3rdpost.png";
import SecondP from "../images/2ndprofile.png";
import ThirdP from "../images/3rdprofile.png";
import FourthP from "../images/4thprofile.png";

const PostWrapper = styled.div`
  .post-filter {
    margin-top: 14px;

    select {
      width: 106px;
      height: 32px;
      border-radius: 4px;
      background: #f1f3f5;
      border: 0;
      outline: none;
      color: #212529;
      font-size: 13px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
    }
  }

  padding: 0;

  .postcount {
    color: #212529;
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    letter-spacing: 0.14px;
    margin: 0;
  }

  @media (min-width: 767px) {
    .post-filter {
      select {
        display: none;
      }
      .postcount {
        display: none;
      }
      .filter-buttons {
        display: flex;
        gap: 10px;
      }
      border-bottom: 1px solid #e0e0e0;
    }
  }
  .filter-buttons,
  .joinbtn {
    button {
      color: #8a8a8a;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      border: 0;
      outline: none;
      background: #fff;
    }
  }
  .joinbtn {
    button {
      color: #000;
      font-size: 15px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      width: 133px;
      height: 36px;
      flex-shrink: 0;
      border-radius: 4px;
      background: #edeef0;
      padding: 0;
      margin: 6px 16px;
    }
    .join {
      border-radius: 4px;
      background: #2f6ce5;
      color: #fff;
    }
  }
  .joingroup {
    margin-top: 28px;
    input {
      overflow: visible;
      border: 0;
      border-bottom: 1px solid #b8b8b8;
    }
    p {
      color: #000;

      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      opacity: 0.5;
      margin-top: 32px;
    }
  }
  .cardcontent {
    padding: 0;
  }
  .row {
    margin-right: 0px;
    margin-left: 0px;
  }
`;

const postcount = [368];

const postdata = [
  {
    id: 1,
    img: PostImg,
    heading: "✍️ Article",
    desc: "What if famous brands had regular fonts? Meet RegulaBrands!",
    smalldesc: "I’ve worked in UX for the better part of a decade. F..",
    username: "Sarthak Kamra",
    workexperience: "",
    userview: "1.4k views",
    profile: "",
    shareprofilelink: "",
    visitapplylink: "",
    visitapplybuttontext: "",
    dateofpost: "",
    location: "",
    profileimg: ProfileImg,
  },
  {
    id: 2,
    img: Secondpost,
    heading: "🔬️ Education",
    desc: "Tax Benefits for Investment under National Pension Scheme launched by Government",
    smalldesc: "I’ve worked in UX for the better part of a decade. F..",
    workexperience: "",
    username: "Sarah West",
    userview: "4.8k views",
    profile: "",
    shareprofilelink: "",
    visitapplybuttontext: "",
    visitapplylink: "",
    dateofpost: "",
    location: "",
    profileimg: SecondP,
  },
  {
    id: 3,
    img: ThirdPost,
    heading: "🗓️ Meetup",
    desc: "Finance & Investment Elite Social Mixer @Lujiazui",
    smalldesc: "",
    workexperience: "",
    username: "Ronal Jones",
    userview: "800 views",
    profile: "",
    shareprofilelink: "",
    visitapplybuttontext: "Visit Website",
    visitapplylink: "Visit Website",
    dateofpost: "Fri, 12 Oct, 2018",
    location: "Ahmedabad, India",
    profileimg: ThirdP,
  },
  {
    id: 4,
    img: "",
    heading: "💼️ Job",
    desc: "Software Developer - II",
    smalldesc: "",
    workexperience: "Innovaccer Analytic...",
    username: "Joseph Gray",
    userview: "1.7k views",
    profile: "",
    shareprofilelink: "",
    visitapplybuttontext: "Apply on Timesjobs",
    visitapplylink: "Visit Website",
    dateofpost: "",
    location: "Noida, India",
    profileimg: FourthP,
  },
];

function Post() {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth > 767);
    };

    handleResize();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <PostWrapper className="postwrapper container">
      <div className="post-filter d-flex justify-content-between container align-items-center justify-content-md-between">
        <p className="postcount">Posts ({postcount})</p>
        {isMobile ? (
          <>
            <div className="filter-buttons">
              <button value="All">All Posts(32)</button>
              <button value="Article">Article</button>
              <button value="Event">Event</button>
              <button value="Education">Education</button>
              <button value="Job">Job</button>
            </div>
            <div className="joinbtn">
              <button value="Education">
                Write a Post{" "}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="22"
                  height="22"
                  viewBox="0 0 22 22"
                  fill="none"
                >
                  <g clip-path="url(#clip0_1_839)">
                    <path
                      d="M6.41663 9.16663L11 13.75L15.5833 9.16663H6.41663Z"
                      fill="black"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_1_839">
                      <rect width="22" height="22" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </button>
              <button value="Job" className="join">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="22"
                  height="22"
                  viewBox="0 0 22 22"
                  fill="none"
                >
                  <g clip-path="url(#clip0_1_859)">
                    <path
                      d="M7.33333 9.16671H4.58333V6.41671H2.75V9.16671H0V11H2.75V13.75H4.58333V11H7.33333V9.16671ZM16.5 10.0834C18.0217 10.0834 19.2408 8.85504 19.2408 7.33337C19.2408 5.81171 18.0217 4.58337 16.5 4.58337C16.2067 4.58337 15.9225 4.62921 15.6658 4.71171C16.1883 5.45421 16.4908 6.35254 16.4908 7.33337C16.4908 8.31421 16.1792 9.20337 15.6658 9.95504C15.9225 10.0375 16.2067 10.0834 16.5 10.0834ZM11.9167 10.0834C13.4383 10.0834 14.6575 8.85504 14.6575 7.33337C14.6575 5.81171 13.4383 4.58337 11.9167 4.58337C10.395 4.58337 9.16667 5.81171 9.16667 7.33337C9.16667 8.85504 10.395 10.0834 11.9167 10.0834ZM17.985 12.0634C18.7458 12.7325 19.25 13.585 19.25 14.6667V16.5H22V14.6667C22 13.255 19.8275 12.3842 17.985 12.0634ZM11.9167 11.9167C10.0833 11.9167 6.41667 12.8334 6.41667 14.6667V16.5H17.4167V14.6667C17.4167 12.8334 13.75 11.9167 11.9167 11.9167Z"
                      fill="white"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_1_859">
                      <rect width="22" height="22" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                Join Group
              </button>
            </div>
          </>
        ) : (
          <select id="dropdown" name="fruit" value="All Posts(32)">
            <option value="All">All Posts(32)</option>
            <option value="Article">Article</option>
            <option value="Event">Event</option>
            <option value="Education">Education</option>
            <option value="Job">Job</option>
          </select>
        )}
      </div>
      <div className="gridcontent row">
        <div className="cardcontent col-md-8">
          <Card post={postdata} />
        </div>
        <div className="joingroup col-md-4">
          <input
            type="text"
            placeholder="Noida, India"
            value="Noida, India"
          ></input>
          <p>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              viewBox="0 0 16 16"
              fill="none"
            >
              <g opacity="0.5" clip-path="url(#clip0_1_853)">
                <path
                  d="M7.33337 10H8.66671V11.3334H7.33337V10ZM7.33337 4.66671H8.66671V8.66671H7.33337V4.66671ZM7.99337 1.33337C4.31337 1.33337 1.33337 4.32004 1.33337 8.00004C1.33337 11.68 4.31337 14.6667 7.99337 14.6667C11.68 14.6667 14.6667 11.68 14.6667 8.00004C14.6667 4.32004 11.68 1.33337 7.99337 1.33337ZM8.00004 13.3334C5.05337 13.3334 2.66671 10.9467 2.66671 8.00004C2.66671 5.05337 5.05337 2.66671 8.00004 2.66671C10.9467 2.66671 13.3334 5.05337 13.3334 8.00004C13.3334 10.9467 10.9467 13.3334 8.00004 13.3334Z"
                  fill="black"
                />
              </g>
              <defs>
                <clipPath id="clip0_1_853">
                  <rect width="16" height="16" fill="white" />
                </clipPath>
              </defs>
            </svg>
            Your location will help us serve better and extend a personalised
            experience.
          </p>
        </div>
      </div>
    </PostWrapper>
  );
}

export default Post;
