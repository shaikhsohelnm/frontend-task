import React, { useState, useEffect } from "react";
import styled from "@emotion/styled";
import PageLogo from "./PageLogo";
import CreateImg from "../images/signin.png";

const WrapperStyled = styled.div`
  margin: 7px 0;
  width: 100%;
  svg {
    width: 162.566px;
    height: 24px;
    flex-shrink: 0;
  }
  .headercontent {
    margin: 0px 72px;
  }
  input {
    max-width: 360px !important;
    height: 42px;
    flex-shrink: 0;
    border-radius: 21px;
    background: #f2f2f2;
    border: 0;
    outline: none;
    margin: 0;
    &:placeholder {
      color: #5c5c5c;
      font-size: 14px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
    }
  }

  @media (min-width: 992px) {
    input {
      width: 360px !important;
    }
  }

  .btn-group {
    button {
      border: 0;
      outline: none;
      background: transparent;
      color: #2e2e2e;
      text-align: right;
      font-size: 16px;
      font-style: normal;
      font-weight: 500;
      span {
        color: #2f6ce5;
        font-size: 16px;
        font-style: normal;
        font-weight: 700;
        line-height: normal;
      }
    }
  }
  .modal-dialog {
    width: 768px !important;
    height: auto !important;
  }
  .modal.show .modal-dialog {
    display: flex !important;
    justify-content: center !important;
    align-items: center !important;
    width: 100vw !important;
    height: 100vh !important;
    margin: auto !important;
  }
  #myModal.modal.show.modal-dialog .modal-content {
    width: 768px !important;
    max-width: 768px !important;
    border-radius: 8px;
    background: #fff;
    box-shadow: 0px 8px 24px 0px rgba(0, 0, 0, 0.25);
  }
  .modal-content .modal-head {
    padding: 17px 58px;
    color: #008a45;
    text-align: center;
    font-size: 14px;
    font-weight: 500;
    line-height: 16px;
    background: #effff4;
    margin: 0;
  }
  .form-group {
    margin: 0px;
    input {
      border-radius: 2px 2px 0px 0px;
      border: 1px solid #d9d9db;
      background: #f7f8fa;
      margin: 0px;
      &:placeholder {
        color: #8a8a8a;
        font-size: 15px;
        font-weight: 500;
        line-height: 16px;
        padding: 15px 12px;
      }
    }
  }
  .modal-body {
    padding: 0px;
    .btn-primary {
      border-radius: 20px;
      background: #2f6ce5;
      border: 0;
      margin-top: 19px;
      width: 320px;
      height: 40px;
    }
  }
  .col {
    padding: 0px;
  }
  .modal-header {
    padding: 0px;
    .modal-title {
      margin-bottom: 24px;
      color: #000;
      font-size: 24px;
      font-weight: 700;
    }
  }
  @media (min-width: 576px) {
    .modal-dialog {
      max-width: 768px !important;
      margin: 1.75rem auto;
    }
  }
  .createaccimg {
    width: 320px;
    height: 320px;
  }
  .row {
    padding: 24px 36px;
    margin-right: 0px;
    margin-left: 0px;
  }
  .modal-footer {
    flex-direction: column;
    align-items: unset;

    button {
      width: 320px;
      height: 38px;
      border-radius: 2px;
      border: 0.6px solid #d9d9db;
      background: #fff;
      margin: 0;
      margin-bottom: 8px;
      padding: 0;
      svg {
        width: unset;
        height: unset;
      }
    }
  }
  .alreadyaccount {
    text-align: right;
    color: #3d3d3d;
    font-size: 13px;
    font-weight: 400;
    button {
      color: #2f6ce5;
      font-weight: 600;
      border: 0;
      outline: none;
      background: inherit;
    }
  }
  .termscondition {
    color: #000;
    text-align: right;
    font-size: 11px;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: -0.088px;
  }
  .modal-header .close {
    padding: 0;
    margin: 0;
    width: 28px;
    height: 28px;
    right: -384px;
    top: -113px;
    svg {
      width: unset;
      height: unset;
    }
  }
`;

function Header() {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <WrapperStyled className="d-flex d-md-block justify-content-end flex-wrap">
        {windowWidth > 767 ? (
          <div className="headercontent d-flex justify-content-between align-items-center">
            <PageLogo />
            <form className="form-inline">
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search for your favorite groups in ATG"
                aria-label="Search"
              />
            </form>
            <div className="btn-group">
              <button
                type="button"
                className="btn btn-secondary dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                onClick={openModal}
              >
                Create account. <span>It’s free!</span>
              </button>
              <div className="dropdown-menu dropdown-menu-right">
                <button
                  className="dropdown-item"
                  type="button"
                  data-toggle="modal"
                  data-target="#myModal"
                >
                  Sign In
                </button>
                <button
                  className="dropdown-item"
                  type="button"
                  data-toggle="modal"
                  data-target="#myModal"
                >
                  Create account
                </button>
              </div>
            </div>
          </div>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="51"
            height="10"
            viewBox="0 0 51 10"
            fill="none"
          >
            {/* Your SVG content */}
          </svg>
        )}

        <div
          className={`modal${showModal ? " show" : ""}`}
          id="myModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden={!showModal}
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content position-relative ">
              <p className="modal-head">
                Let's learn, share & inspire each other with our passion for
                computer engineering. Sign up now 🤘🏼
              </p>
              <div className="row">
                <div className="col">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">
                      Create Account
                    </h5>
                    <button
                      type="button"
                      className="close position-absolute"
                      data-dismiss="modal"
                      aria-label="Close"
                      onClick={closeModal}
                    >
                      <span aria-hidden="true">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="28"
                          height="28"
                          viewBox="0 0 28 28"
                          fill="none"
                        >
                          <g clip-path="url(#clip0_1_2255)">
                            <path
                              d="M14 2.33331C7.54838 2.33331 2.33337 7.54831 2.33337 14C2.33337 20.4516 7.54838 25.6666 14 25.6666C20.4517 25.6666 25.6667 20.4516 25.6667 14C25.6667 7.54831 20.4517 2.33331 14 2.33331ZM19.8334 18.1883L18.1884 19.8333L14 15.645L9.81171 19.8333L8.16671 18.1883L12.355 14L8.16671 9.81164L9.81171 8.16665L14 12.355L18.1884 8.16665L19.8334 9.81164L15.645 14L19.8334 18.1883Z"
                              fill="white"
                            />
                          </g>
                          <defs>
                            <clipPath id="clip0_1_2255">
                              <rect width="28" height="28" fill="white" />
                            </clipPath>
                          </defs>
                        </svg>
                      </span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div class="form-group ">
                        <input
                          type="email"
                          class="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                          placeholder="First Name"
                        />
                        <input
                          type="email"
                          class="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                          placeholder="Last Name"
                        />
                      </div>
                      <div class="form-group">
                        <input
                          type="email"
                          class="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                          placeholder="Email"
                        />
                      </div>
                      <div class="form-group">
                        <input
                          type="password"
                          class="form-control"
                          id="exampleInputPassword1"
                          placeholder="Password"
                        />
                      </div>
                      <div class="form-group">
                        <input
                          type="password"
                          class="form-control"
                          id="exampleInputPassword1"
                          placeholder="Confirm Password"
                        />
                      </div>
                      <button
                        type="submit"
                        className="btn btn-primary createaccount"
                      >
                        Create Account
                      </button>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="" data-dismiss="modal">
                      Sign up with Facebook
                    </button>

                    <button type="button" className="" data-dismiss="modal">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        viewBox="0 0 16 16"
                        fill="none"
                      >
                        <g clip-path="url(#clip0_1_2217)">
                          <path
                            d="M3.54594 9.66905L2.989 11.7482L0.953406 11.7912C0.345063 10.6629 0 9.37192 0 8.00005C0 6.67345 0.322625 5.42245 0.8945 4.32092H0.894938L2.70719 4.65317L3.50106 6.45455C3.33491 6.93895 3.24434 7.45895 3.24434 8.00005C3.24441 8.5873 3.35078 9.14995 3.54594 9.66905Z"
                            fill="#FBBB00"
                          />
                          <path
                            d="M15.8602 6.50562C15.9521 6.98955 16 7.48933 16 8.00012C16 8.57287 15.9398 9.13155 15.8251 9.67046C15.4357 11.5043 14.4181 13.1056 13.0084 14.2388L13.008 14.2384L10.7253 14.1219L10.4023 12.1052C11.3377 11.5566 12.0687 10.6981 12.4537 9.67046H8.1759V6.50562H12.5161H15.8602Z"
                            fill="#518EF8"
                          />
                          <path
                            d="M13.0081 14.2382L13.0085 14.2386C11.6375 15.3406 9.89596 15.9999 8.00015 15.9999C4.95355 15.9999 2.30477 14.2971 0.953552 11.7911L3.54608 9.66895C4.22168 11.472 5.96102 12.7555 8.00015 12.7555C8.87662 12.7555 9.69774 12.5186 10.4023 12.105L13.0081 14.2382Z"
                            fill="#28B446"
                          />
                          <path
                            d="M13.1064 1.84175L10.5148 3.9635C9.78553 3.50769 8.92353 3.24438 8.00003 3.24438C5.91475 3.24438 4.14288 4.58678 3.50113 6.4545L0.894969 4.32088H0.894531C2.22597 1.75384 4.90816 0 8.00003 0C9.94112 0 11.7209 0.691438 13.1064 1.84175Z"
                            fill="#F14336"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0_1_2217">
                            <rect width="16" height="16" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                      Sign up with Google
                    </button>
                  </div>
                </div>
                <div className="col">
                  <p className="alreadyaccount">
                    Already have an account? <button> Sign In</button>
                  </p>
                  <img src={CreateImg} alt="" className="createaccimg" />
                  <p className="termscondition">
                    By signing up, you agree to our Terms & conditions, Privacy
                    policy
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </WrapperStyled>
    </>
  );
}

export default Header;
