import React, { useEffect, useState } from "react";
import styled from "@emotion/styled";

const CardWrapper = styled.div`
  margin-top: 20px;
  box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.12);
  .card-body {
    .card-title {
      font-size: 14px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      margin-bottom: 6px;
    }
    .card-text {
      font-size: 16px;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
      color: #212529;
      margin-bottom: 10px;
    }
    .card-text.small {
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      color: #495057;
    }
    .headbutton {
      button {
        outline: none;
        border: 0;
        background: #fff;
      }
    }
  }
  .profileimg {
    img {
      width: 37px;
      height: 37px;
      flex-shrink: 0;
      border-radius: 24px;
    }
  }
  .profilecontent {
    margin-left: 12px;
    p {
      margin: 0;
      color: #212529;
      font-size: 13px;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
    }
    p.view {
      font-size: 12px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      color: #495057;
    }
  }
  .sharebtn {
    button {
      width: 70px;
      height: 36px;
      border-radius: 4px;
      background: #f1f3f5;
      border: 0;
      outline: none;
      color: #212529;
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      margin-left: 4px;
    }
  }
  .postdatelocation {
    p {
      color: #212529;
      font-size: 12px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      letter-spacing: -0.12px;
    }
  }
`;

function Card({ post = [] }) {
  return (
    <>
      {post.map((ele) => (
        <CardWrapper className="card" key={ele.id}>
          {ele.img && (
            <img className="card-img-top" src={ele.img} alt="Card image cap" />
          )}
          <div className="card-body">
            <h5 className="card-title">{ele.heading}</h5>
            <div className="headbutton d-flex justify-content-between">
              <p className="card-text">{ele.desc}</p>
              <button className="">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="28"
                  height="28"
                  viewBox="0 0 28 28"
                  fill="none"
                >
                  <g clip-path="url(#clip0_1_38)">
                    <path
                      d="M18.6666 14C18.6666 15.2834 19.7166 16.3334 20.9999 16.3334C22.2833 16.3334 23.3333 15.2834 23.3333 14C23.3333 12.7167 22.2833 11.6667 20.9999 11.6667C19.7166 11.6667 18.6666 12.7167 18.6666 14ZM16.3333 14C16.3333 12.7167 15.2833 11.6667 13.9999 11.6667C12.7166 11.6667 11.6666 12.7167 11.6666 14C11.6666 15.2834 12.7166 16.3334 13.9999 16.3334C15.2833 16.3334 16.3333 15.2834 16.3333 14ZM9.33325 14C9.33325 12.7167 8.28325 11.6667 6.99992 11.6667C5.71659 11.6667 4.66659 12.7167 4.66659 14C4.66659 15.2834 5.71658 16.3334 6.99992 16.3334C8.28325 16.3334 9.33325 15.2834 9.33325 14Z"
                      fill="#212529"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_1_38">
                      <rect
                        width="28"
                        height="28"
                        fill="white"
                        transform="translate(28) rotate(90)"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </button>
            </div>

            <p className="card-text small">{ele.smalldesc}</p>
            <div className="postdatelocation d-flex justify-content-between flex-wrap">
              {ele.dateofpost && (
                <p>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_1_70)">
                      <path
                        d="M12.6667 2.00001H12V0.666672H10.6667V2.00001H5.33333V0.666672H4V2.00001H3.33333C2.59333 2.00001 2.00667 2.6 2.00667 3.33334L2 12.6667C2 13.4 2.59333 14 3.33333 14H12.6667C13.4 14 14 13.4 14 12.6667V3.33334C14 2.6 13.4 2.00001 12.6667 2.00001ZM12.6667 12.6667H3.33333V5.33334H12.6667V12.6667ZM4.66667 6.66667H8V10H4.66667V6.66667Z"
                        fill="#495057"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_1_70">
                        <rect width="16" height="16" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>
                  {ele.dateofpost}
                </p>
              )}

              {ele.workexperience && (
                <p>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_1_131)">
                      <path
                        d="M9.33325 4.00002V2.66668H6.66659V4.00002H9.33325ZM2.66659 5.33335V12.6667H13.3333V5.33335H2.66659ZM13.3333 4.00002C14.0733 4.00002 14.6666 4.59335 14.6666 5.33335V12.6667C14.6666 13.4067 14.0733 14 13.3333 14H2.66659C1.92659 14 1.33325 13.4067 1.33325 12.6667L1.33992 5.33335C1.33992 4.59335 1.92659 4.00002 2.66659 4.00002H5.33325V2.66668C5.33325 1.92668 5.92659 1.33335 6.66659 1.33335H9.33325C10.0733 1.33335 10.6666 1.92668 10.6666 2.66668V4.00002H13.3333Z"
                        fill="#495057"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_1_131">
                        <rect width="16" height="16" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>
                  {ele.workexperience}
                </p>
              )}
              {ele.location && (
                <p>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_1_61)">
                      <path
                        d="M7.99992 1.33333C5.41992 1.33333 3.33325 3.42 3.33325 6C3.33325 9.5 7.99992 14.6667 7.99992 14.6667C7.99992 14.6667 12.6666 9.5 12.6666 6C12.6666 3.42 10.5799 1.33333 7.99992 1.33333ZM4.66659 6C4.66659 4.16 6.15992 2.66666 7.99992 2.66666C9.83992 2.66666 11.3333 4.16 11.3333 6C11.3333 7.92 9.41325 10.7933 7.99992 12.5867C6.61325 10.8067 4.66659 7.9 4.66659 6Z"
                        fill="#495057"
                      />
                      <path
                        d="M7.99992 7.66666C8.92039 7.66666 9.66659 6.92047 9.66659 5.99999C9.66659 5.07952 8.92039 4.33333 7.99992 4.33333C7.07944 4.33333 6.33325 5.07952 6.33325 5.99999C6.33325 6.92047 7.07944 7.66666 7.99992 7.66666Z"
                        fill="#495057"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_1_61">
                        <rect width="16" height="16" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>{" "}
                  {ele.location}
                </p>
              )}
            </div>
            <div className="profile d-flex justify-content-between">
              <div className="profileimg d-flex">
                <img src={ele.profileimg} alt="" />
                <div className="profilecontent">
                  <p>{ele.username}</p>
                  <p className="view"> {ele.userview} </p>
                </div>
              </div>
              <div className="sharebtn">
                <button>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                  >
                    <g clip-path="url(#clip0_1_46)">
                      <path
                        d="M13.5 12.06C12.93 12.06 12.42 12.285 12.03 12.6375L6.6825 9.525C6.72 9.3525 6.75 9.18 6.75 9C6.75 8.82 6.72 8.6475 6.6825 8.475L11.97 5.3925C12.375 5.7675 12.9075 6 13.5 6C14.745 6 15.75 4.995 15.75 3.75C15.75 2.505 14.745 1.5 13.5 1.5C12.255 1.5 11.25 2.505 11.25 3.75C11.25 3.93 11.28 4.1025 11.3175 4.275L6.03 7.3575C5.625 6.9825 5.0925 6.75 4.5 6.75C3.255 6.75 2.25 7.755 2.25 9C2.25 10.245 3.255 11.25 4.5 11.25C5.0925 11.25 5.625 11.0175 6.03 10.6425L11.37 13.7625C11.3325 13.92 11.31 14.085 11.31 14.25C11.31 15.4575 12.2925 16.44 13.5 16.44C14.7075 16.44 15.69 15.4575 15.69 14.25C15.69 13.0425 14.7075 12.06 13.5 12.06Z"
                        fill="#212529"
                      />
                    </g>
                    <defs>
                      <clipPath id="clip0_1_46">
                        <rect width="18" height="18" fill="white" />
                      </clipPath>
                    </defs>
                  </svg>
                  Share
                </button>
              </div>
            </div>
          </div>
        </CardWrapper>
      ))}
    </>
  );
}

export default Card;
