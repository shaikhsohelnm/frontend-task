import React from "react";
import Header from "../components/Header";
import LandingImg from "../images/Rectangle2.png";
import styled from "@emotion/styled";
import Post from "../components/Post";
import Landingdesktop from "../images/landingdesktop.png";

const LandingWrapper = styled.div`
  width: 100%;
  img {
    width: 100%;
    height: 236px;
  }
  @media only screen and (min-width: 525px) {
    img {
      height: auto;
    }
  }
  .landing-content1 {
    top: 15px;
    display: flex;
    justify-content: space-between;
    left: 0;
    right: 0;

    .btn {
      border-radius: 4px;
      border: 0.8px solid #fff;
      color: #fff;
      font-size: 12px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      height: 28px;
    }
  }

  .landing-content2 {
    bottom: 32px;
    h1 {
      color: #fff;
      font-size: 17px;
      font-style: normal;
      font-weight: 700;
      line-height: normal;
      margin: 0;
      margin-bottom: 2px;
    }
    p {
      color: #fff;
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: normal;
      margin: 0;
    }
  }
  @media (min-width: 767px) {
    .landing-content2 {
      left: 0;
      right: 0;
      h1 {
        font-size: 36px;
        font-weight: 700;
      }
      p {
        font-size: 18px;
        font-weight: 400;
      }
    }
  }
`;

function Landing() {
  return (
    <>
      <Header />
      <LandingWrapper className="landing-section position-relative">
        <picture>
          <source srcSet={Landingdesktop} media="(min-width: 768px)" />
          <img src={LandingImg} alt="Description" />
        </picture>
        <div className="landing-content1 position-absolute container">
          <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <g id="icon/navigation/arrow_back_24px">
              <path
                id="icon/navigation/arrow_back_24px_2"
                d="M20 11H7.83L13.42 5.41L12 4L4 12L12 20L13.41 18.59L7.83 13H20V11Z"
                fill="white"
              />
            </g>
          </svg>
          <button className="btn">Join Group</button>
        </div>
        <div className="landing-content2 position-absolute container">
          <h1>Computer Engineering</h1>
          <p>142,765 Computer Engineers follow this</p>
        </div>
      </LandingWrapper>
      <Post />
    </>
  );
}

export default Landing;
